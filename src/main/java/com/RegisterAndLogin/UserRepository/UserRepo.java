package com.RegisterAndLogin.UserRepository;

import com.RegisterAndLogin.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepo extends JpaRepository<User, Integer> {

    Optional<User> findByEmailAndPassword(String login, String password);
}
