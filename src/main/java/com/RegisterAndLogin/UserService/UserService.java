package com.RegisterAndLogin.UserService;

import com.RegisterAndLogin.Model.User;
import com.RegisterAndLogin.UserRepository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private final UserRepo userRepository;

    public UserService(UserRepo userRepository) {
        this.userRepository = userRepository;
    }

    public User loginUser(String login, String password, String email){
        if (!(login != null & password != null)) {
            return null;
        } else {
            User userModel = new User();
            userModel.setLogin(login);
            userModel.setPassword(password);
            userModel.setEmail(email);
            return userRepository.save(userModel);
        }
    }
    public User authenticate(String login, String password){
        return userRepository.findByEmailAndPassword(login, password).orElse(null);
    }
}
